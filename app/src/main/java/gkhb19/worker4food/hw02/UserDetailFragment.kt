package gkhb19.worker4food.hw02

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.*
import android.widget.ScrollView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import gkhb19.worker4food.hw02.data.*
import gkhb19.worker4food.hw02.ext.*
import kotlinx.android.synthetic.main.fragment_user_detail.*

class UserDetailFragment(private val showNav: Boolean = false) : Fragment() {

    private val theActivity
        get() = activity as AppCompatActivity

    private val infoViews by lazy { listOf(
        user_avatar, user_full_name,
        phone_icon, user_phone,
        email_icon, user_email,
        country_icon, user_country,
        city_icon, user_city,
        address_icon, user_address,
        send_location
    )}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_user_detail, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        theActivity.supportActionBar?.setDisplayHomeAsUpEnabled(showNav)

        val model = activityViewModels<UserModel>()
        model.value.selectedUser.observe(this, Observer(::onUserChange))

        send_location.setOnClickListener {
            Intent(this.context, LocationActivity::class.java).also {
                val user = model.value.selectedUser.value!!
                val args = LocationActivityArgs(
                    resources.getString(R.string.text_user_name, user.name.first, user.name.last),
                    context!!.userAvatarThumbId(user))

                it.putExtras(args.toBundle())
                startActivity(it)
            }
        }
    }

    override fun onDetach() {
        theActivity.supportActionBar?.setDisplayHomeAsUpEnabled(!showNav)

        super.onDetach()
    }

    private fun onUserChange(u: User?) {

        infoViews.forEach { it.visibility = if(u == null) View.GONE else View.VISIBLE }

        no_user.visibility = if(u == null) View.VISIBLE else View.GONE

        if(u != null) {
            user_avatar.setImageResource(context!!.userAvatarId(u))

            user_full_name.text = resources.getString(R.string.text_user_full_name,
                u.name.title,
                u.name.first,
                u.name.last
            )

            user_phone.text = u.cell
            user_email.text = u.email
            user_country.text = u.location.country
            user_city.text = u.location.city

            user_address.text = resources.getString(R.string.text_location_address,
                u.location.postcode,
                u.location.street.name,
                u.location.street.number
            )

            // root_container is always null, WHY?
            (user_detail_view.parent as ScrollView).fullScroll(ScrollView.FOCUS_UP)
        }
    }
}
