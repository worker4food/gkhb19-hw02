package gkhb19.worker4food.hw02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.lifecycle.*
import gkhb19.worker4food.hw02.data.*
import kotlinx.android.synthetic.main.activity_main.land_user_detail

class MainActivity : AppCompatActivity() {

    private val model by viewModels<UserModel>()

    private val listFragment by lazy(::UserListFragment)

    private val detailFragment by lazy { UserDetailFragment(true) }

    private val isPortrait
        get() = land_user_detail?.isInLayout?.not() ?: true

    private val currentFragment
        get() = if(model.selectedUser.value == null) listFragment else detailFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(isPortrait)
            model.selectedUser.observe(this, Observer { onUserChange() })
    }

    private fun onUserChange() =
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_container, currentFragment)
            .commit()

    override fun onBackPressed() =
        if(isPortrait && currentFragment == detailFragment)
            model.selectedUser.value = null
        else
            super.onBackPressed()

    override fun onOptionsItemSelected(item: MenuItem) =
        when(item.itemId) {
            android.R.id.home ->  {
                model.selectedUser.value = null
                true
            }
            else -> false
        }
}
