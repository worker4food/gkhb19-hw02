package gkhb19.worker4food.hw02

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import gkhb19.worker4food.hw02.data.UserModel

class UserListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater
        .inflate(R.layout.fragment_user_list, container, false)
        .let { it as RecyclerView }
        .apply {
            layoutManager = LinearLayoutManager(context)
            adapter = UserListAdapter(activityViewModels<UserModel>().value)
        }
}
