package gkhb19.worker4food.hw02.ext

import android.content.Context
import gkhb19.worker4food.hw02.R
import gkhb19.worker4food.hw02.data.User

fun Context.userAvatarId(u: User)
        = getDrawableId(u.picture.large, R.drawable.no_avatar_128)

fun Context.userAvatarThumbId(u: User)
        = getDrawableId(u.picture.thumbnail, R.drawable.no_avatar_48)

private fun Context.getDrawableId(name: String, default: Int) = resources
        .getIdentifier(name, "drawable", packageName)
        .let { if(it == 0) default else it }
