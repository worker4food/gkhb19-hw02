package gkhb19.worker4food.hw02

import android.view.*
import androidx.recyclerview.widget.RecyclerView
import gkhb19.worker4food.hw02.data.UserModel
import kotlinx.android.synthetic.main.fragment_user_list_item.view.*
import gkhb19.worker4food.hw02.ext.*

class UserListAdapter(private val model: UserModel)
    : RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
        = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_user_list_item, parent, false)
            .run(::ViewHolder)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position)

    override fun getItemCount(): Int = model.users.size

    inner class ViewHolder(view: View)
        : RecyclerView.ViewHolder(view) {

        init {
            itemView.setOnClickListener {
                model.selectedUser.value = model.users[adapterPosition]
            }
        }

        fun bind(pos: Int) {
            val user = model.users[pos]

            with(itemView) {
                avatar_thumb.setImageResource(context.userAvatarThumbId(user))

                user_name.text = resources.getString(
                    R.string.text_user_name,
                    user.name.first,
                    user.name.last
                )

                user_location.text = resources.getString(
                    R.string.text_location_summary,
                    user.location.city,
                    user.location.country
                )
            }
        }
    }
}
