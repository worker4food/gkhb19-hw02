package gkhb19.worker4food.hw02

import android.Manifest
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.crashlytics.android.Crashlytics
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.activity_location.*

class LocationActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_ACCESS = 42
        const val CHECK_SETTINGS = 84
        const val TEN_SECONDS = 10_000L

        var notificationId = 1
    }

    private val channelId by lazy { packageName }

    private val requiredPerms = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    private val locationReq
        get() = LocationRequest.create().apply {
            interval = TEN_SECONDS
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

    private val locationClient: FusedLocationProviderClient? by lazy { LocationServices.getFusedLocationProviderClient(this) }

    private lateinit var lastLoc: Location

    private val userInfo by lazy { LocationActivityArgs.fromBundle(intent?.extras!!)}

    private val locationCallback = object: LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)

            locationResult?.lastLocation?.also {
                lastLoc = it

                showLocation()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                getString(R.string.channel_name),
                NotificationManager.IMPORTANCE_UNSPECIFIED
            )

            getSystemService(NotificationManager::class.java)
                .createNotificationChannel(channel)
        }

        notifyBtn.setOnClickListener {
            with(NotificationManagerCompat.from(this)) {
                val builder = NotificationCompat.Builder(this@LocationActivity, channelId)
                    .setSmallIcon(R.drawable.no_avatar_48)
                    .setContentTitle("To: ${userInfo.name}")
                    .setContentText(infoText.text)

                notify(notificationId++, builder.build())
            }
        }
    }

    override fun onResume() {
        super.onResume()

        val accessGranted = requiredPerms.any {
            checkSelfPermission(it) == PackageManager.PERMISSION_GRANTED
        }

        if(!accessGranted) {
            requestPermissions(requiredPerms, REQUEST_ACCESS)
            return
        }

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationReq)

        LocationServices.getSettingsClient(this)
            .checkLocationSettings(builder.build())
            .addOnFailureListener {
                if(it is ResolvableApiException) {
                    it.startResolutionForResult(this, CHECK_SETTINGS)
                } else {
                    Crashlytics.logException(it)

                    Toast.makeText(
                        this,
                        getString(R.string.all_wrong),
                        Toast.LENGTH_LONG
                    ).show()

                    finish()
                }
            }
            .addOnSuccessListener {
                locationClient?.requestLocationUpdates(locationReq, locationCallback, Looper.myLooper())
            }
    }

    override fun onPause() {
        super.onPause()

        locationClient?.removeLocationUpdates(locationCallback)
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if(requestCode == REQUEST_ACCESS) {
            val accessGranted = grantResults.any { it == PackageManager.PERMISSION_GRANTED }

            if(!accessGranted) {
                Toast.makeText(applicationContext, R.string.no_perms, Toast.LENGTH_SHORT)
                    .apply {
                        setGravity(1, 0, 0)
                    }
                    .show()

                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == CHECK_SETTINGS && resultCode != Activity.RESULT_OK) {
            finish()
        }
    }

    private fun showLocation() {
        progressBar.visibility = View.GONE
        infoText.visibility = View.VISIBLE
        notifyBtn.visibility = View.VISIBLE

        infoText.text = getString(R.string.coordinates, lastLoc.latitude, lastLoc.longitude)
    }
}
