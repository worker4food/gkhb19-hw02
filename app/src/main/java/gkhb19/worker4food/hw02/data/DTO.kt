package gkhb19.worker4food.hw02.data

import kotlinx.serialization.*

@Serializable
data class User (
    val gender: String,
    val name: Name,
    val location: Location,
    val email: String,
    val dob: DateAge,
    val registered: DateAge,
    val phone: String,
    val cell: String,
    val picture: Picture,
    val nat: String
)

@Serializable
data class DateAge (
    val date: String,
    val age: Long
)

@Serializable
data class Location (
    val street: Street,
    val city: String,
    val state: String,
    val country: String,
    val postcode: String,
    val coordinates: Coordinates,
    val timezone: Timezone
)

@Serializable
data class Coordinates (
    val latitude: String,
    val longitude: String
)

//@Serializable
//sealed class Postcode {
//    @Serializable
//    class Digit(val value: Long)  : Postcode()
//    @Serializable
//    class Alpha(val value: String) : Postcode()
//}

@Serializable
data class Street (
    val number: Long,
    val name: String
)

@Serializable
data class Timezone (
    val offset: String,
    val description: String
)

@Serializable
data class Name (
    val title: String,
    val first: String,
    val last: String
)

@Serializable
data class Picture (
    val large: String,
    val medium: String,
    val thumbnail: String
)
