package gkhb19.worker4food.hw02.data

import android.app.Application
import androidx.lifecycle.*
import gkhb19.worker4food.hw02.R
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.list

class UserModel(app: Application) : AndroidViewModel(app) {
    val users: List<User> by lazy {
        getApplication<Application>().resources
            .openRawResource(R.raw.data)
            .bufferedReader(Charsets.UTF_8)
            .use { it.readText() }
            .let {
                Json(JsonConfiguration.Stable)
                    .parse(User.serializer().list, it)
            }
    }

    var selectedUser = MutableLiveData<User?>().apply { value = null }
}
