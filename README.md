# Geekhub 2019 - android

## Homework 02

### Project status
 - [x] Different layouts for landscape and portrait
 - [x] Show user list
 - [x] Avatars in list
 - [x] Detailed information about user
 
 [Link to .apk](https://bitbucket.org/worker4food/gkhb19-hw02/downloads/app-debug.apk)
